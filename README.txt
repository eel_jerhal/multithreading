TAREA 1 SISTEMAS OPERATIVOS

Jeremy Castro // jeremy.castroe@alumnos.uv.cl
Felipe Leviñir // felipe.levinir@alumnos.uv.cl

Para resolver el problema planteado en la tarea, inicialmente se creó la función de busqueda (contarParcial) a la cual se le pasaron 
2 parametros de tipo entero para marcar un inicio y final de las secciones del arreglo a recorrer en donde se usa una variable global llamada totalSuma para sumar 
la cantidad de veces que se encuentra la letra.
La idea principal es que a este metodo se le hacen llamados a través de threads para lograr dividir el problema en partes que se ejecutan de manera paralela-concurrente, 
esta división se realiza por total de elementos con el numero de threads(totalElementos / numThreads) y para evitar los problemas con numeros decimales se procede a redondear 
la división por ejemplo 1000/6 = 166.6666.... redondeado es = 166 ahora se le resta del total de elementos la multiplicación de ese número por la cantidad de threads - 1 
ejemplo 166*5 = 830, 1000-830 = 170 ahora el número obtenido es la cantidad de elementos que tendrá el último hilo.
Se tienen 6 hilos, donde 5 tienen 166 elementos y el último 170.
De esta manera no hay perdida de datos al asignar las secciones a recorrer.
Finalmente se procede a unir el trabajo realizado mediante la funcion join para voler a unir la ejecucion de la busqueda y obtener el total.

En una pequeña conclusión nos dimos cuenta de que el dividir el problema en n threads mayores a las que posee el hardware y además de tener un listado de elementos pequeño, 
no existe ganancia de tiempo, es más se tarda más que hacerlo de manera secuencial.
