# Taller 01: 
==========
Contar en forma paralela/concurrente las apariciones de un caracter en un arreglo, utilizando threads en C++14

# Objetivos:
==========

Crear un programa que llene de un arreglo de letras aleatorias en forma secuencial y que luego cuente en forma paralela/concurrente las apariciones de una letra determinada. 

# Forma de uso:
=============
```console
./contar -n <nro> -t <nro>  -l <letra> [-h]

    Parámetros:
    	-n   : tamaño del arreglo.
    	-t   : número de threads.
    	-l   : letra a buscar
    	[-h] : muestra la ayuda de uso y termina.
```

 
# Ejemplo de uso:
===============

```console
$cd <directorio_proyecto>
$make clean
$ make
$ ./contar -n 1000 -t 2 -l a
```